package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;

import com.example.demo.controller.MainController;

@SpringBootApplication
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SpringSecurityApplication {

	public static void main(String[] args) {
		System.out.println("aaaaa");
		SpringApplication.run(SpringSecurityApplication.class, args);
	}
}
